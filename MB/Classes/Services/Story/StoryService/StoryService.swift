//
//  StoryService.swift
//  MB
//
//  Created by piton
//  Copyright © 2016 anukhov. All rights reserved.
//

import RxSwift

protocol StoryService {
    
    func obtainFromApi() throws -> Observable<StoryList>
    
    func obtainFromCache() throws -> StoryList
    
    func clearCache() throws
}
