//
//  StoryServiceImplementation.swift
//  MB
//
//  Created by piton
//  Copyright © 2016 anukhov. All rights reserved.
//

import Foundation
import RealmSwift
import RxSwift

struct StoryServiceImplementation: StoryService {
 
    var storyApi: StoryApi!
    var storyStorage: StoryStorage!

    func obtainFromApi() throws -> Observable<StoryList> {
        
        return try storyApi.obtainStories()
            .map {
                try self.clearCache()
                try self.storyStorage.update(stories: $0)                
                return $0
            }
    }
    
    func obtainFromCache() throws -> StoryList {
        return try storyStorage.obtainStories()
    }
    
    func clearCache() throws {
        
        try storyStorage.clear()
    }
}
