//
//  StoryStorage.swift
//  MB
//
//  Created by piton
//  Copyright © 2016 anukhov. All rights reserved.
//

import RxSwift

protocol StoryStorage {
    
    func obtainStories() throws -> StoryList
    
    func update(stories: StoryList) throws
    
    func clear() throws
}
