//
//  StoryStorageImplementation.swift
//  MB
//
//  Created by piton
//  Copyright © 2016 anukhov. All rights reserved.
//

import RealmSwift
import RxSwift

struct StoryStorageImplementation: StoryStorage {

    #if DEBUG
    
    static let realmPath: Any = {
        print("REALM: ", (try? Realm().configuration.fileURL) as Any)
    }()
    
    init() {
        _ = StoryStorageImplementation.realmPath
    }
    
    #endif
    
    func obtainStories() throws -> StoryList {
        
        let realm = try Realm()
        
        let results = realm.objects(StoryModel.self)

        return results.map { $0 }
    }
    
    func update(stories: StoryList) throws {
        
        let realm = try Realm()
        
        try realm.write() {
            realm.add(stories, update: true)
        }
    }
    
    func clear() throws {
        
        let realm = try Realm()
        
        try realm.write {
            realm.deleteAll()
        }
    }
}
