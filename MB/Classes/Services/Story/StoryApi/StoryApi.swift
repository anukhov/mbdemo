//
//  StoryApi.swift
//  MB
//
//  Created by piton
//  Copyright © 2016 anukhov. All rights reserved.
//

import RxSwift

protocol StoryApi {
    
    func obtainStories() throws -> Observable<StoryList>
}
