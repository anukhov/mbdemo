//
//  StoryApiImplementation.swift
//  MB
//
//  Created by piton
//  Copyright © 2016 anukhov. All rights reserved.
//

import Foundation
import SwiftyJSON
import RxSwift

struct StoryApiImplementation: StoryApi {

    // TODO: Make fabric
    
    private let url = URL(string: "http://kly.webtm.ru/api/index.php/v1/stories")!
    
    private var session: URLSession {
        return URLSession.shared
    }
    
    func obtainStories() throws -> Observable<StoryList> {
        
        return session.rx
            .json(url: url)
            .map { try StoryListMapper(json: JSON($0)).value }
    }
}

