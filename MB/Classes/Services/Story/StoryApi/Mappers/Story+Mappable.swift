//
//  StoryModel+Mappable.swift
//  MB
//
//  Created by piton
//  Copyright © 2016 anukhov. All rights reserved.
//

import SwiftyJSON

extension StoryModel: Mappable {
    
    convenience init(json: JSON) throws {
        
        // TODO: add mappers
        
        guard let id = json["id"].string else {
            throw MappableErrors.badFormat
        }
        
        let text = json["description"].stringValue
        let title = json["title"].stringValue
        let link = json["link"].string
        let date = Date(timeIntervalSince1970: json["pubDate"].doubleValue)
        
        try self.init(id: id, title: title, text: text, date: date, link: link)
    }
}

