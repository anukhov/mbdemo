//
//  StoryListMapper.swift
//  MB
//
//  Created by piton
//  Copyright © 2016 anukhov. All rights reserved.
//

import SwiftyJSON

struct StoryListMapper: MappableList {
    
    let value: StoryList
    
    init(json: JSON) throws {
        
        // print(json)
        
        guard let list = json["stories"].array else {
            throw MappableErrors.badFormat
        }
        
        value = try list.map{ try StoryModel(json: $0) }
    }    
}
