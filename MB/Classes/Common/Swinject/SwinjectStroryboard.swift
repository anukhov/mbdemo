//
//  SwinjectStroryboard.swift
//  MB
//
//  Created by piton
//  Copyright © 2016 anukhov. All rights reserved.
//

import Swinject
import SwinjectStoryboard

extension SwinjectStoryboard {

    class func setup() {
       
        // TODO:

        func registerModule<C: Container.Controller, A: ModuleAssembly>(_ controllerType: C.Type, _ assemblyType: A.Type) {
            defaultContainer.registerForStoryboard(controllerType) { r, c in
                (r as? Container)?.register(controllerType) { [unowned c] _ in c }
                r.resolve(assemblyType)?.assemble(resolver: r)
            }
        }
        
        registerModule(StoryListViewController.self, StoryListAssembly.self)
        registerModule(StoryDetailsViewController.self, StoryDetailsAssembly.self)
        
        // MARK: -
        
        defaultContainer.register(StoryDetailsAssembly.self) { _ in
            StoryDetailsAssembly()
        }

        defaultContainer.register(StoryListAssembly.self) { _ in
            StoryListAssembly()
        }

        // MARK: -
        
        defaultContainer.register(StoryService.self) { r in
            var service = StoryServiceImplementation()
            service.storyApi = r.resolve(StoryApi.self)
            service.storyStorage = r.resolve(StoryStorage.self)
            return service
        }
        
        defaultContainer.register(StoryApi.self) { _ in
            StoryApiImplementation()
        }

        defaultContainer.register(StoryStorage.self) { _ in
            StoryStorageImplementation()
        }
    }
}
