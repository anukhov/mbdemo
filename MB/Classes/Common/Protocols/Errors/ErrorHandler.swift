//
//  File.swift
//  MB
//
//  Created by piton
//  Copyright © 2016 anukhov. All rights reserved.
//

protocol ErrorHandler {
    
    func errorOccured(error: Error)
}

import UIKit

extension ErrorHandler where Self: UIViewController {
    
    fileprivate func show(error: Error) {
        
        print("ERROR:", error)
        
        let alert = UIAlertController(
            title: "Error",
            message: String(describing: error),
            preferredStyle: .alert
        )
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        present(alert, animated: true, completion: nil)
    }
    
    func errorOccured(error: Error) {
        show(error: error)
    }
}

extension ErrorHandler where Self: UIViewController, Self: ViewLoadingSupport {

    func errorOccured(error: Error) {
        show(error: error)
        finishLoading()
    }
}
