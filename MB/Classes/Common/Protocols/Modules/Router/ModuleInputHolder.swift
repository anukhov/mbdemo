//
//  ModuleInputHolder.swift
//  MB
//
//  Created by piton
//  Copyright © 2016 anukhov. All rights reserved.
//

protocol ModuleInputHolder {
    
    var moduleInput: ModuleInput? { get }
}
