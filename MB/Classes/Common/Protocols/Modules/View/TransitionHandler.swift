//
//  TransitionHandler.swift
//  MB
//
//  Created by piton
//  Copyright © 2016 anukhov. All rights reserved.
//

import UIKit

protocol TransitionHandler: class {

    func openModule(sequeInfo: SegueInfo)
    
    func closeModule()
}

extension TransitionHandler where Self: UIViewController {
    
    func openModule(sequeInfo: SegueInfo) {
        performSegue(withIdentifier: sequeInfo.identifier, sender: sequeInfo)
    }
    
    func closeModule() {}
}

extension TransitionHandler where Self: UIViewController {
    
    func prepareSegueInfo(for segue: UIStoryboardSegue, sender: Any?) {
        guard let moduleInput = (segue.destination as? ModuleInputHolder)?.moduleInput else {
            return
        }
        (sender as? SegueInfo)?.configuration?(moduleInput)
    }
}

