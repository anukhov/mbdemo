//
//  UserInteractionSuppot.swift
//  MB
//
//  Created by piton
//  Copyright © 2016 anukhov. All rights reserved.
//

import  UIKit

// MARK: - 

protocol InteractionElement: class {
    var isEnabled: Bool { get set }
}

extension UIBarItem: InteractionElement {}
extension UIButton: InteractionElement {}

protocol UserInteractionSuppot {
    
    var userInteractionElements: [InteractionElement] { get }
    
    func enableUserInteraction()
  
    func disableUserInteraction()
}

extension UserInteractionSuppot {

    var userInteractionElements: [InteractionElement] {
        return []
    }
    
    func enableUserInteraction() {
        userInteractionElements.forEach {
            $0.isEnabled = true
        }
    }
    
    func disableUserInteraction() {
        userInteractionElements.forEach {
            $0.isEnabled = false
        }
    }
}
