//
//  SpinnerSupport.swift
//  MB
//
//  Created by piton
//  Copyright © 2016 anukhov. All rights reserved.
//

import UIKit

// MARK: - Spinner

protocol Spinner {
    
    var spinnerView: UIView { get }
}

extension Spinner where Self: UIView {
    
    var spinnerView: UIView {
        return self
    }
}

extension UIView: Spinner {}

// MARK: - SpinnerSupport

protocol SpinnerSupport {
    
    var spinner: Spinner { get }
    
    func showSpinner()
    
    func hideSpinner()
}

extension SpinnerSupport where Self: UIViewController {
    
    private var spinnerView: UIView {
        return spinner.spinnerView
    }
    
    func showSpinner() {
        view.addSubview(spinnerView)
        
        if let scroll = view as? UIScrollView {
            scroll.setContentOffset(CGPoint(x: 0, y: -scroll.contentInset.top), animated: false)
            scroll.isScrollEnabled = false
        }
        
        spinnerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        spinnerView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        spinnerView.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
        spinnerView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
    }
    
    func hideSpinner() {
        spinnerView.removeFromSuperview()
        (view as? UIScrollView)?.isScrollEnabled = true
    }
}
