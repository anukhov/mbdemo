//
//  ViewLoadingSupport.swift
//  MB
//
//  Created by piton
//  Copyright © 2016 anukhov. All rights reserved.
//

protocol ViewLoadingSupport {
    
    func startLoading()
    
    func finishLoading()
}

extension ViewLoadingSupport where Self: SpinnerSupport & UserInteractionSuppot {
    
    func startLoading() {
        disableUserInteraction()
        showSpinner()
    }
    
    func finishLoading() {
        enableUserInteraction()
        hideSpinner()
    }
}
