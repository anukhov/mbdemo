//
//  ModuleAssembly.swift
//  MB
//
//  Created by piton
//  Copyright © 2016 anukhov. All rights reserved.
//

import Swinject

protocol ModuleAssembly {
    func assemble(resolver: ResolverType)
}

