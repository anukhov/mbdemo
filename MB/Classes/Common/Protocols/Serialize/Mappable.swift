//
//  Mappable.swift
//  MB
//
//  Created by piton
//  Copyright © 2016 anukhov. All rights reserved.
//

import Foundation
import SwiftyJSON

enum MappableErrors: Error {
    case badFormat
}

protocol Mappable: class {
    
    init(json: JSON) throws
}

protocol MappableList {
    
    associatedtype T: Mappable
    
    var value: [T] { get }
    
    init(json: JSON) throws
}
