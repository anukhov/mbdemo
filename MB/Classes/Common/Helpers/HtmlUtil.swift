//
//  HtmlUtil.swift
//  MB
//
//  Created by piton on 21.11.16.
//  Copyright © 2016 anukhov. All rights reserved.
//

import UIKit

struct HtmlUtil {
    
    func convert(html: String?, fontSize: CGFloat? = nil) -> NSAttributedString? {

        var same: NSAttributedString? {
            return NSAttributedString(string: html ?? "")
        }
        
        guard let html = html else {
            return same
        }
        
        var style = ""
        
        if let fontSize = fontSize {
            style =
                "<style>" +
                    "body{font-size:\(fontSize)px;}" +
            "</style>"
        }
        
        let fullHtml = "\(style)<body>\(html)</body>"
        
        guard let data = fullHtml.data(using: .unicode),
            let attributedString = try? NSAttributedString(
                data: data,
                options: [
                    NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                    NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue
                ],
                documentAttributes: nil
            ) else {
                return same
        }
        
        return NSMutableAttributedString(attributedString: attributedString)
    }
}

