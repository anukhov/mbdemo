//
//  StoryPlainObject.swift
//  MB
//
//  Created by piton
//  Copyright © 2016 anukhov. All rights reserved.
//

import Foundation

struct StoryPlainObject {
    
    let id: StoryId
    
    var title: String?
    
    var text: String?
    
    var date: Date?
    
    var link: String?
    
    var url: URL? {
        return URL(string: link ?? "")
    }
    
    init(model: StoryModel) {
        self.id = model.id
        self.title = model.title
        self.text = model.text
        self.date = model.date
        self.link = model.link
    }
}

