//
//  StoryModel.swift
//  MB
//
//  Created by piton
//  Copyright © 2016 anukhov. All rights reserved.
//

import Foundation
import RealmSwift

typealias StoryId = String
typealias StoryList = [StoryModel]

final class StoryModel: Object {
    
    dynamic var id: StoryId = ""
    dynamic var title: String?
    dynamic var text: String?
    dynamic var date: Date?
    dynamic var link: String?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    // MARK: -
    
    convenience init(id: StoryId, title: String?, text: String?, date: Date?, link: String?) throws {
        guard !id.isEmpty else {
            throw ModelErrors.idError
        }
        self.init()
        self.id = id
        self.title = title
        self.text = text
        self.date = date
        self.link = link
    }
    
    convenience init(storyItem: StoryPlainObject) {
        self.init()
        self.id = storyItem.id
        self.title = storyItem.title
        self.text = storyItem.text
        self.date = storyItem.date
        self.link = storyItem.link
    }

    // MARK: -
    
    func plainObject() -> StoryPlainObject {
        return StoryPlainObject(model: self)
    }
}

