//
//  SegueInfo.swift
//  MB
//
//  Created by piton
//  Copyright © 2016 anukhov. All rights reserved.
//

typealias ModuleInputConfiguration = (ModuleInput) -> ()

struct SegueInfo {
    
    var identifier: String
    var configuration: ModuleInputConfiguration?
}
