//
//  StoryListInteractorInput.swift
//  MB
//
//  Created by piton
//  Copyright © 2016 anukhov. All rights reserved.
//

protocol StoryListInteractorInput: class {
    
    func obtainFromApi()
    
    func obtainFromCache()
    
    func clearCache()
}
