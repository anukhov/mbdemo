//
//  StoryListInteractor.swift
//  MB
//
//  Created by piton
//  Copyright © 2016 anukhov. All rights reserved.
//

import Foundation
import RxSwift
import RealmSwift

class StoryListInteractor {
    
    weak var output: StoryListInteractorOutput?
    
    var storyService: StoryService!
    
    fileprivate let disposeBag = DisposeBag()
    
    fileprivate func errorOccured(error: Error) {
        DispatchQueue.main.async {
            self.output?.errorOccured(error: error)
        }
    }
}

extension StoryListInteractor: StoryListInteractorInput {
    
    func obtainFromCache() {
        
        print(#function)
        
        DispatchQueue.global().async {
            
            sleep(1) // TODO: REMOVE: for demo only
            
            do {
                let stories = try self.storyService
                    .obtainFromCache()
                    .map { $0.plainObject() }
                
                DispatchQueue.main.async {
                    self.output?.didObtain(stories: stories)
                }
            } catch {
                self.errorOccured(error: error)
            }
        }
    }

    func obtainFromApi() {
        
        print(#function)
                
        DispatchQueue.global().async {

            sleep(1) // TODO: REMOVE: for demo only
            
            do {
                
                try self.storyService
                    .obtainFromApi()
                    .map { $0.map { $0.plainObject() } }
                    .subscribe(onNext: { _ in
                        self.obtainFromCache()
                    },onError: {
                        self.errorOccured(error: $0)
                    })
                    .addDisposableTo(self.disposeBag)
                
            } catch {
                self.errorOccured(error: error)
            }
        }
    }
    
    func clearCache() {
        
        print(#function)
        
        DispatchQueue.global().async {
            
            sleep(1) // TODO: REMOVE: for demo only
            
            do {
                try self.storyService?.clearCache()
            } catch {
                self.errorOccured(error: error)
                return
            }

            DispatchQueue.main.async {
                self.output?.didClearCache()
            }
        }
    }
}
