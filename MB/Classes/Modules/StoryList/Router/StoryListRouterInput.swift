//
//  StoryListRouterInput.swift
//  MB
//
//  Created by piton
//  Copyright © 2016 anukhov. All rights reserved.
//

protocol StoryListRouterInput {
    
    func openStoryDetailsModule(storyItem: StoryPlainObject)
}
