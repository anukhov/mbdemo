//
//  StoryListRouter.swift
//  MB
//
//  Created by piton
//  Copyright © 2016 anukhov. All rights reserved.
//

class StoryListRouter: StoryListRouterInput {
    
    weak var transitionHandler: TransitionHandler?
    
    func openStoryDetailsModule(storyItem: StoryPlainObject) {

        let segueInfo = SegueInfo(identifier: "OpenStoryDetails") {
            ($0 as? StoryDetailsModuleInput)?.configure(storyItem: storyItem)
        }

        transitionHandler?.openModule(sequeInfo: segueInfo)
    }
}
