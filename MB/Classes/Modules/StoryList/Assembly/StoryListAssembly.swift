//
//  StoryListAssembly.swift
//  MB
//
//  Created by piton
//  Copyright © 2016 anukhov. All rights reserved.
//

import Swinject

class StoryListAssembly: ModuleAssembly {
    
    func assemble(resolver: ResolverType) {
        
        let view = resolver.resolve(StoryListViewController.self)
        
        let router = StoryListRouter()
        router.transitionHandler = view
        
        let presenter = StoryListPresenter()
        presenter.view = view
        presenter.router = router
        
        let interactor = StoryListInteractor()
        interactor.output = presenter
        interactor.storyService = resolver.resolve(StoryService.self)
        
        presenter.interactor = interactor
        
        view?.output = presenter
    }
}
