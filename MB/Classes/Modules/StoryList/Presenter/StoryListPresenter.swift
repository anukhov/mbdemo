//
//  StoryListPresenter.swift
//  MB
//
//  Created by piton
//  Copyright © 2016 anukhov. All rights reserved.
//

class StoryListPresenter {
    
    weak var view: StoryListViewInput?
    var interactor: StoryListInteractorInput?
    var router: StoryListRouterInput?
}

extension StoryListPresenter: StoryListViewOutput {
    
    func setupView() {
        view?.startLoading()
        interactor?.obtainFromCache()
    }
    
    func didTap(storyItem: StoryPlainObject) {
        router?.openStoryDetailsModule(storyItem: storyItem)
    }
    
    func didTapFromApiButton() {
        view?.startLoading()
        interactor?.obtainFromApi()
    }
    
    func didTapFromCacheButton() {
        view?.startLoading()
        interactor?.obtainFromCache()
    }
    
    func didTapClearCacheButton() {
        view?.startLoading()
        interactor?.clearCache()
    }
}

extension StoryListPresenter: StoryListInteractorOutput {
    
    func didObtain(stories: [StoryPlainObject]) {
        view?.finishLoading()
        view?.update(stories: stories)
    }
    
    func didClearCache() {
        view?.finishLoading()
        view?.update(stories: [])
    }
    
    func errorOccured(error: Error) {
        view?.errorOccured(error: error)
    }
}
