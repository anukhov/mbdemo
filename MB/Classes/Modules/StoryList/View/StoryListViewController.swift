//
//  StoryListViewController.swift
//  MB
//
//  Created by piton
//  Copyright © 2016 anukhov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class StoryListViewController: UITableViewController {
    
    var output: StoryListViewOutput?
    
    @IBOutlet var spinnerView: UIView!
    
    fileprivate var tableManager: StoryListViewTableManager!
    
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableManager = StoryListViewTableManager(tableView: tableView) { [weak self] in
            self?.output?.didTap(storyItem: $0)
        }
        
        output?.setupView()
    }
    
    // MARK: -
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        prepareSegueInfo(for: segue, sender: sender)
    }
    
    // MARK: -
    
    @IBAction func fromApiAction(_ sender: Any) {
        output?.didTapFromApiButton()
    }
    
    @IBAction func fromCacheAction(_ sender: Any) {
        output?.didTapFromCacheButton()
    }
    
    @IBAction func claerCacheAction(_ sender: Any) {
        output?.didTapClearCacheButton()
    }
}

extension StoryListViewController: SpinnerSupport {
    
    var spinner: Spinner {
        return spinnerView
    }
}

extension StoryListViewController: UserInteractionSuppot {

    var userInteractionElements: [InteractionElement] {
        return (navigationItem.leftBarButtonItems ?? [])
            + (navigationItem.rightBarButtonItems ?? [])
    }
}

extension StoryListViewController: StoryListViewInput {
    
    func update(stories: [StoryPlainObject]) {
        tableManager.stories = stories
        print(#function, stories.count)
    }
}



