//
//  StoryListViewInput.swift
//  MB
//
//  Created by piton
//  Copyright © 2016 anukhov. All rights reserved.
//

protocol StoryListViewInput: class, ViewLoadingSupport, TransitionHandler, ErrorHandler {
    
    func update(stories: [StoryPlainObject])
}
