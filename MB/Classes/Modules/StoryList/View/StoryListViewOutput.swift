//
//  StoryListViewOutput.swift
//  MB
//
//  Created by piton
//  Copyright © 2016 anukhov. All rights reserved.
//

protocol StoryListViewOutput {
    
    func didTap(storyItem: StoryPlainObject)
    func setupView()
    
    func didTapFromApiButton()
    func didTapFromCacheButton()
    func didTapClearCacheButton()
}
