//
//  StoryCell.swift
//  MB
//
//  Created by piton on 21.11.16.
//  Copyright © 2016 anukhov. All rights reserved.
//

import UIKit

class StoryCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var linkLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    var story: StoryPlainObject? {
        didSet {
           
            titleLabel.text = story?.title
            
            descLabel.attributedText = HtmlUtil().convert(html: story?.text, fontSize: descLabel.font.pointSize)
            
            linkLabel.text = story?.link

            var dateFormat: String? {                
                if let date = story?.date {
                    let df = DateFormatter()
                    df.dateStyle = .medium
                    return df.string(from: date)
                } else {
                    return nil
                }
            }
            
            dateLabel.text = dateFormat
        }
    }
}
