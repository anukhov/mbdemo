//
//  StoryListViewTableManager.swift
//  MB
//
//  Created by piton
//  Copyright © 2016 anukhov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class StoryListViewTableManager {
    
    private let tableView: UITableView
    
    private let storyVariable = Variable<[StoryPlainObject]>([])
    
    private let disposeBag = DisposeBag()
    
    private subscript (idx: Int) -> StoryPlainObject? {
        return 0..<stories.count ~= idx ? stories[idx] : nil
    }

    // MARK: -
    
    var stories: [StoryPlainObject] {
        get {
            return storyVariable.value
        }
        set {
            storyVariable.value = newValue
        }
    }
    
    init(tableView: UITableView, onSelect: ((StoryPlainObject) -> ())?) {
        
        self.tableView = tableView
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
        
        storyVariable.asObservable()
            .bindTo(tableView.rx.items(cellIdentifier: "StoryCell", cellType: StoryCell.self)) { _, model, cell in
                cell.story = model
            }
            .addDisposableTo(disposeBag)
        
        tableView.rx.itemSelected
            .subscribe(onNext: {
                guard let item = self[$0.row] else {
                    return
                }
                onSelect?(item)
            })
            .addDisposableTo(disposeBag)
    }
}
