//
//  StoryDetailsModuleInput.swift
//  MB
//
//  Created by piton
//  Copyright © 2016 anukhov. All rights reserved.
//

protocol StoryDetailsModuleInput: ModuleInput {
    
    func configure(storyItem: StoryPlainObject)
}
