//
//  StoryDetailsPresenter.swift
//  MB
//
//  Created by piton
//  Copyright © 2016 anukhov. All rights reserved.
//

class StoryDetailsPresenter {
    
    deinit { print(#function, self) }
   
    weak var view: StoryDetailsViewInput?
    var interactor: StoryDetailsInteractorInput?
    var router: StoryDetailsRouterInput?
    
    fileprivate lazy var state: StoryDetailsState = {
        return StoryDetailsState()
    }()
    
    fileprivate var currentStoryItem: StoryPlainObject? {
        get {
            return state.storyItem
        }
        set {
            state.storyItem = newValue
        }
    }
}

extension StoryDetailsPresenter: StoryDetailsModuleInput {
    
    func configure(storyItem: StoryPlainObject) {
        currentStoryItem = storyItem
    }
}

extension StoryDetailsPresenter: StoryDetailsViewOutput {
    
    func setupView() {
        view?.update(storyItem: currentStoryItem)
    }    
}

extension StoryDetailsPresenter: StoryDetailsInteractorOutput {
    
    func didUpdate(storyItem: StoryPlainObject) {
        currentStoryItem = storyItem
        view?.finishLoading()
        view?.update(storyItem: storyItem)
    }
    
    func errorOccured(error: Error) {
        view?.errorOccured(error: error)
    }
}
