//
//  StoryDetailsInteractorOutput.swift
//  MB
//
//  Created by piton
//  Copyright © 2016 anukhov. All rights reserved.
//

protocol StoryDetailsInteractorOutput: class, ErrorHandler {
    
    func didUpdate(storyItem: StoryPlainObject)
}

