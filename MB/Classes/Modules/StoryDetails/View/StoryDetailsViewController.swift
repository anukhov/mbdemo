//
//  StoryDetailsViewController.swift
//  MB
//
//  Created by piton
//  Copyright © 2016 anukhov. All rights reserved.
//

import UIKit

class StoryDetailsViewController: UIViewController {

    deinit { print(#function, self) }
    
    var output: StoryDetailsViewOutput?
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet var spinnerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        output?.setupView()
    }
}

extension StoryDetailsViewController: SpinnerSupport {
    
    var spinner: Spinner {
        return spinnerView
    }
}

extension StoryDetailsViewController: UserInteractionSuppot {}

extension StoryDetailsViewController: StoryDetailsViewInput {
    
    func update(storyItem: StoryPlainObject?) {
        print(#function, storyItem?.link ?? "")
        title = storyItem?.link
        guard let url = storyItem?.url else {
            return
        }
        let request = URLRequest(url: url)
        webView.loadRequest(request)
    }
}

extension StoryDetailsViewController: ModuleInputHolder {
    
    var moduleInput: ModuleInput? {
        return output as? ModuleInput
    }
}

