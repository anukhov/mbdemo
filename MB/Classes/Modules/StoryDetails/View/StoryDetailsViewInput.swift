//
//  StoryDetailsViewInput.swift
//  MB
//
//  Created by piton
//  Copyright © 2016 anukhov. All rights reserved.
//

protocol StoryDetailsViewInput: class, ViewLoadingSupport, TransitionHandler, ErrorHandler {
    
    func update(storyItem: StoryPlainObject?)
}
