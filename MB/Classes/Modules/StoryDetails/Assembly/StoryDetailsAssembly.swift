//
//  StoryDetailsAssembly.swift
//  MB
//
//  Created by piton
//  Copyright © 2016 anukhov. All rights reserved.
//

import Swinject

class StoryDetailsAssembly: ModuleAssembly {
    
    deinit { print(#function, self) }
    
    func assemble(resolver: ResolverType) {
        
        let view = resolver.resolve(StoryDetailsViewController.self)
        
        let router = StoryDetailsRouter()
        router.transitionHandler = view
        
        let presenter = StoryDetailsPresenter()
        presenter.view = view
        presenter.router = router
        
        let interactor = StoryDetailsInteractor()
        interactor.output = presenter
        interactor.storyService = resolver.resolve(StoryService.self)
        
        presenter.interactor = interactor
        
        view?.output = presenter
    }
}
